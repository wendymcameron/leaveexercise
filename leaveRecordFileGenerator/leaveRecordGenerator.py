from __future__ import print_function
import datetime as dt

import random
import csv

__author__ = 'wendycameron'

'''
This python module generates a sampleLeaveData.csv file making the assumptions that you have:
1) 100,000 Employess
2) An employee will take a random number of holidays between 1 and 4
3) Each holiday will be of a random duration of between 1 and 20 days.
'''
teams = ['a-team', 'b-team', 'c-team', 'd-team','e-team', 'f-team','g-team', 'h-team','i-team', 'j-team','k-team', 'l-team','m-team', 'n-team','o-team', 'p-team', 'q-team', 'r-team','s-team', 't-team','u-team', 'v-team', 'x-team', 'y-team', 'z-team']

if __name__ == '__main__':
    with open('sampleLeaveData.csv', 'wb') as csvfile:
        sampleDataWriter = csv.writer(csvfile, delimiter=',',
                                      quotechar='"', quoting=csv.QUOTE_ALL)
        sampleDataWriter.writerow(['Employee_Name', 'Employee_Team', 'Start_Date', 'End_Date'])

        #Generate leave records for 100,000 employees
        for x in range(100000):
            team = teams[random.randint(0, len(teams)-1)]
            #Generate between 1 and 4 holidays
            for y in range(random.randint(1, 4)):
                start_date = dt.datetime(2014, 1, 1, 0, 0) + dt.timedelta(days=random.randrange(0, 365))
                # Deal with weekends by plus or minusing a day
                if start_date.weekday() == 5:
                    start_date = start_date - dt.timedelta(days=-1)
                elif start_date.weekday() == 6:
                    start_date = start_date - dt.timedelta(days=1)
                #Generate a random holiday duration of up to 20 days.
                end_date = start_date + dt.timedelta(days=random.randint(1, 20))
                # Deal with weekends by plus or minusing a day
                if end_date.weekday() == 5:
                    end_date = start_date - dt.timedelta(days=-1)
                elif end_date.weekday() == 6:
                    end_date = start_date - dt.timedelta(days=1)
                #Write the csv file row with a generated employee id and start and end data for the leave record
                sampleDataWriter.writerow(["employee-x={}".format(x), team,start_date.isoformat(), end_date.isoformat()])
    '''
    DONT RUN THIS AGAIN FOR SAFETY COMMENTING

    with open('sampleLeaveDataTest.csv', 'wb') as csvfile:
        sampleDataWriter = csv.writer(csvfile, delimiter=',',
                                      quotechar='"', quoting=csv.QUOTE_ALL)
        sampleDataWriter.writerow(['Employee_Name', 'Employee_Team', 'Start_Date', 'End_Date'])

        #Generate leave records for 100,000 employees
        for x in range(2000):
            team = teams[random.randint(0, len(teams)-1)]
            #Generate between 1 and 4 holidays
            for y in range(random.randint(1, 4)):
                start_date = dt.datetime(2014, 1, 1, 0, 0) + dt.timedelta(days=random.randrange(0, 365))
                if start_date.weekday() == 5:
                    start_date = start_date - dt.timedelta(days=-1)
                elif start_date.weekday() == 6:
                    start_date = start_date - dt.timedelta(days=1)
                #Generate a random holiday duration of up to 20 days.
                end_date = start_date + dt.timedelta(days=random.randint(1, 20))
                if end_date.weekday() == 5:
                    end_date = start_date - dt.timedelta(days=-1)
                elif end_date.weekday() == 6:
                    end_date = start_date - dt.timedelta(days=1)
                #Write the csv file row with a generated employee id and start and end data for the leave record
                sampleDataWriter.writerow(["employee-x={}".format(x), team,start_date.isoformat(), end_date.isoformat()])
    '''