#!/bin/sh
cd $(dirname $0)
mvn clean package
java -jar target/leave-0.1.0.jar &
PID=$!
sleep 180
curl -s http://localhost:8080/teams > target/teamsActual.json
curl -s http://localhost:8080/employees > target/employeesActual.json
curl -s http://localhost:8080/employees/a-team > target/a-teamActual.json
curl -s http://localhost:8080/leave/intervals/employee-x=94 > target/employee-x=94Actual.json
curl -s "http://localhost:8080/leave/intervals/team/c-team?startDate=2014/01/01&endDate=2014/06/30&absences=1" > target/absencesActual.json

kill -9 $PID

exit
