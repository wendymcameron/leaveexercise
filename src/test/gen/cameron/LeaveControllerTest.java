package gen.cameron;

import org.apache.commons.logging.LogFactory;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.PropertySource;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultHandler;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.servlet.View;

import java.util.logging.Level;
import java.util.logging.LogManager;
import java.util.logging.Logger;

import static org.hamcrest.Matchers.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;

/**
 * Created by wendycameron on 26/09/2014.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@SpringApplicationConfiguration(classes = Application.class)
@IntegrationTest("server.port:8080")
@ContextConfiguration(classes = Application.class)
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class,
        DirtiesContextTestExecutionListener.class,
        TransactionalTestExecutionListener.class})
@ComponentScan
public class LeaveControllerTest {

    @Autowired
    private WebApplicationContext ctx;

    //    @Autowired
//    private LeaveController leaveController;
//
//    @Mock
//    private View mockView;
//
    private MockMvc mockMvc;

    @Autowired
    private LeaveLoader leaveLoader;

    private static boolean setUp;

    @Before
    public void setUp() throws Exception {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(ctx).build();
        if (setUp == false){
            setUp = true ;
            leaveLoader.loadLeaveRecordsFromCsvFile();
        }

    }

    @Test
    public void testShouldRetrieveAllTeams() throws Exception {
        this.mockMvc.perform(get("/teams").accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType("application/json;charset=UTF-8"))
                .andExpect(jsonPath("$[*].teamName", containsInAnyOrder("a-team",
                        "b-team", "c-team", "d-team", "e-team", "f-team", "g-team",
                        "h-team", "i-team", "j-team", "k-team", "l-team", "m-team",
                        "n-team", "o-team", "p-team", "q-team", "r-team", "s-team",
                        "t-team", "u-team", "v-team", "x-team", "y-team", "z-team"
                )));
    }

    @Test
    public void testShouldRetrieveAllEmployees() throws Exception {
        String[] employees = new String[2000];
        for (int i = 0; i < 2000; i++) {
            employees[i] = "employee-x=" + i;
        }
        this.mockMvc.perform(get("/employees").accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType("application/json;charset=UTF-8"))
                .andExpect(jsonPath("$[*].employeeName", containsInAnyOrder(employees)));
    }

    @Test
    public void testShouldRetreiveEmployeesForTeam() throws Exception {
        this.mockMvc.perform(get("/employees/a-team").accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType("application/json;charset=UTF-8"))
                .andExpect(jsonPath("$[*].employeeName", containsInAnyOrder("employee-x=26",
                        "employee-x=116",
                        "employee-x=148",
                        "employee-x=164",
                        "employee-x=197",
                        "employee-x=268",
                        "employee-x=303",
                        "employee-x=345",
                        "employee-x=382",
                        "employee-x=397",
                        "employee-x=399",
                        "employee-x=401",
                        "employee-x=443",
                        "employee-x=448",
                        "employee-x=457",
                        "employee-x=467",
                        "employee-x=470",
                        "employee-x=520",
                        "employee-x=608",
                        "employee-x=622",
                        "employee-x=652",
                        "employee-x=663",
                        "employee-x=689",
                        "employee-x=718",
                        "employee-x=720",
                        "employee-x=744",
                        "employee-x=769",
                        "employee-x=809",
                        "employee-x=814",
                        "employee-x=852",
                        "employee-x=861",
                        "employee-x=874",
                        "employee-x=880",
                        "employee-x=889",
                        "employee-x=916",
                        "employee-x=944",
                        "employee-x=960",
                        "employee-x=970",
                        "employee-x=982",
                        "employee-x=1067",
                        "employee-x=1097",
                        "employee-x=1100",
                        "employee-x=1108",
                        "employee-x=1223",
                        "employee-x=1259",
                        "employee-x=1266",
                        "employee-x=1276",
                        "employee-x=1288",
                        "employee-x=1294",
                        "employee-x=1298",
                        "employee-x=1332",
                        "employee-x=1364",
                        "employee-x=1368",
                        "employee-x=1386",
                        "employee-x=1415",
                        "employee-x=1428",
                        "employee-x=1448",
                        "employee-x=1468",
                        "employee-x=1490",
                        "employee-x=1548",
                        "employee-x=1615",
                        "employee-x=1669",
                        "employee-x=1710",
                        "employee-x=1798",
                        "employee-x=1831",
                        "employee-x=1853",
                        "employee-x=1878",
                        "employee-x=1939",
                        "employee-x=1982")));
    }

    @Test
    public void testShouldReturnLeaveForAGivenEmployee() throws Exception {
//        "employee-x=21","m-team","2014-07-12T00:00:00","2014-07-23T00:00:00"
//        "employee-x=21","m-team","2014-03-12T00:00:00","2014-03-22T00:00:00"
        this.mockMvc.perform(get("/leave/intervals/employee-x=21").accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType("application/json;charset=UTF-8"))
                .andExpect(jsonPath("$[0].employee.employeeName", is("employee-x=21")))
                .andExpect(jsonPath("$[0].employee.team.teamName", is("m-team")))
                .andExpect(jsonPath("$[0].startDate", is("2014-07-12")))
                .andExpect(jsonPath("$[0].endDate", is("2014-07-23")))
                .andExpect(jsonPath("$[1].employee.employeeName", is("employee-x=21")))
                .andExpect(jsonPath("$[1].employee.team.teamName", is("m-team")))
                .andExpect(jsonPath("$[1].startDate", is("2014-03-12")))
                .andExpect(jsonPath("$[1].endDate", is("2014-03-22")));
    }

    @Test
    public void testShouldReturnAbsenceCountForPeriodAndTeam() throws Exception {
        this.mockMvc.perform(get("/leave/intervals/team/a-team")
                .accept(MediaType.APPLICATION_JSON)
                .param("absences", "2")
                .param("startDate", "2014/01/01")
                .param("endDate", "2014/06/30"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[*].employeeCount", everyItem(greaterThan(2))))
                .andDo(print());
    }
}