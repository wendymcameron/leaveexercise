package gen.cameron;

import com.google.common.collect.Lists;
import gen.cameron.model.Employee;
import gen.cameron.model.Leave;
import gen.cameron.model.LeaveCount;
import gen.cameron.model.Team;
import gen.cameron.repository.EmployeeRepository;
import gen.cameron.repository.LeaveCountRepository;
import gen.cameron.repository.LeaveRepository;
import gen.cameron.repository.TeamRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

/**
 * Created by wendycameron on 25/09/2014.
 */
@RestController
public class LeaveController {

    private static final String template = "Hello, %s!";
    private final AtomicLong counter = new AtomicLong();

    @Autowired
    private TeamRepository teamRepository;

    @Autowired
    private EmployeeRepository employeeRepository;

    @Autowired
    private LeaveRepository leaveRepository;

    @Autowired
    private LeaveCountRepository leaveCountRepository;

    /**
     > a) List of holiday intervals (startdate, end date) for any given employee
     > b) List of employees for a given team
     > c) List of All teams in the system.
     > d) dates in a given period when, for a given team , more than X number of people have booked
     */
    /**
     *
     * @return
     */

    @RequestMapping(value = "/leave/intervals/team/{team}", method = RequestMethod.GET)
    @ResponseBody
    public List<LeaveCount> leave(
            @PathVariable(value="team") String team,
            @RequestParam(value="absences") int absences,
            @RequestParam(value="startDate") Date startDate,
            @RequestParam(value="endDate") Date endDate
    ) {
        return Lists.newArrayList(leaveCountRepository.findByTeamAndDayBetweenAndEmployeeCountGreaterThan(team, startDate, endDate, absences));
    }

    @RequestMapping(value = "/leave/intervals/{employeeName}", method = RequestMethod.GET)
    @ResponseBody
    public List<Leave> intervals(@PathVariable("employeeName") String employeeName){
        return Lists.newArrayList(leaveRepository.findByEmployeeName(employeeName));
    }

    @RequestMapping(value="/employees/{team}", method = RequestMethod.GET)
    @ResponseBody
    public List<Employee> employeesForTeam(@PathVariable("team") String team){
        return employeeRepository.findByTeamName(team);
    }

    @RequestMapping(value = "/employees", method = RequestMethod.GET)
    @ResponseBody
    public List<Employee> employees(){
        return Lists.newArrayList(employeeRepository.findAll());
    }

    @RequestMapping(value = "/teams", method = RequestMethod.GET)
    @ResponseBody
    public List<Team> teams(){
        return Lists.newArrayList(teamRepository.findAll());
    }
}
