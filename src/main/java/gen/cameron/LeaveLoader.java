package gen.cameron;

import gen.cameron.model.Employee;
import gen.cameron.model.Leave;
import gen.cameron.model.LeaveCount;
import gen.cameron.model.Team;
import gen.cameron.repository.EmployeeRepository;
import gen.cameron.repository.LeaveCountRepository;
import gen.cameron.repository.LeaveRepository;
import gen.cameron.repository.TeamRepository;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.mapping.DefaultLineMapper;
import org.springframework.batch.item.file.transform.DelimitedLineTokenizer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.FileSystemResource;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

/**
 * Created by wendycameron on 25/09/2014.
 */
@Component
public class LeaveLoader {

    private String filename;
    private DateFormat m_ISO8601Local = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");

    @Autowired
    private LeaveCountRepository leaveDayRepository;

    @Autowired
    private EmployeeRepository employeeRepository;

    @Autowired
    private TeamRepository teamRepository;

    @Autowired
    private LeaveRepository leaveRepository;

    @Autowired
    public LeaveLoader(@Value("${filename}") String filename) {
        if (!StringUtils.isEmpty(filename)) {
            this.filename = filename;
        }
    }

    public void loadLeaveRecordsFromCsvFile() {
        FlatFileItemReader<LeaveRecord> itemReader = new FlatFileItemReader<LeaveRecord>();
        itemReader.setLinesToSkip(1);
        itemReader.setResource(new FileSystemResource(this.filename));

        DefaultLineMapper<LeaveRecord> lineMapper = new DefaultLineMapper<LeaveRecord>();
        lineMapper.setLineTokenizer(new DelimitedLineTokenizer());
        lineMapper.setFieldSetMapper(new LeaveRecord.LeaveRecordSetMapper());
        itemReader.setLineMapper(lineMapper);
        itemReader.open(new ExecutionContext());
        try {
            LeaveRecord leaveRecord = itemReader.read();
            while (leaveRecord != null) {
                Team team = teamRepository.findByTeamName(leaveRecord.getTeam());
                if (team == null) {
                    team = new Team();
                    team.setTeamName(leaveRecord.getTeam());
                    teamRepository.save(team);
                }
                Employee employee = employeeRepository.findByEmployeeName(leaveRecord.getEmployeeName());
                if (employee == null) {
                    employee = new Employee();
                    employee.setEmployeeName(leaveRecord.getEmployeeName());
                    //An employee can only be in one team (its a simplification)
                    employee.setTeam(team);
                    employeeRepository.save(employee);
                }
                Leave leave = new Leave();
                leave.setEmployee(employee);
                leave.setStartDate(leaveRecord.getStartDate());
                leave.setEndDate(leaveRecord.getEndDate());
                leaveRepository.save(leave);
                mapReduceRecord(leaveRecord, team);
                leaveRecord = itemReader.read();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Transforms the leave record into an in memory Redis map of dates associated with
     * lists of employees on leave
     *
     * @param leaveRecord
     */
    private void mapReduceRecord(LeaveRecord leaveRecord, Team team) {
        int difInDays = (int) ((leaveRecord.getEndDate().getTime() - leaveRecord.getStartDate().getTime()) / (1000 * 60 * 60 * 24));
        Calendar c = Calendar.getInstance();
        c.setTime(leaveRecord.getStartDate());
        for (int i = 0; i < difInDays; i++) {
            //Add the leave records into the in memory db for usage in the api.
            c.add(Calendar.DATE, i);
            List<LeaveCount> days = leaveDayRepository.findByDayAndTeam(c.getTime(), leaveRecord.getTeam());
            LeaveCount day = null;
            if (days.isEmpty()) {
                day = new LeaveCount();
                day.setDay(c.getTime());
                day.setTeam(team);
            } else {
                day = days.get(0);
            }
            int count = day.getEmployeeCount() + 1;
            day.setEmployeeCount(count);
            leaveDayRepository.save(day);
        }
    }
}
