package gen.cameron.model;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by wendycameron on 26/09/2014.
 */
@Entity
public class LeaveCount {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private long id;

    @Temporal(TemporalType.DATE)
    private Date day;

    @ManyToOne
    private Team team;

    private int employeeCount;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Date getDay() {
        return day;
    }

    public void setDay(Date day) {
        this.day = day;
    }

    public int getEmployeeCount() {
        return employeeCount;
    }

    public void setEmployeeCount(int employeeCount) {
        this.employeeCount = employeeCount;
    }

    public Team getTeam() {
        return team;
    }

    public void setTeam(Team team) {
        this.team = team;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        LeaveCount leaveDay = (LeaveCount) o;

        if (employeeCount != leaveDay.employeeCount) return false;
        if (id != leaveDay.id) return false;
        if (day != null ? !day.equals(leaveDay.day) : leaveDay.day != null) return false;
        if (team != null ? !team.equals(leaveDay.team) : leaveDay.team != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + (day != null ? day.hashCode() : 0);
        result = 31 * result + (team != null ? team.hashCode() : 0);
        result = 31 * result + employeeCount;
        return result;
    }
}
