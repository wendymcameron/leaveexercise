package gen.cameron;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.PropertySource;

import java.util.logging.Level;
import java.util.logging.Logger;

@ComponentScan
@PropertySource("classpath:/application.properties")
@EnableAutoConfiguration
public class Application implements CommandLineRunner {

    @Autowired
    private LeaveLoader leaveLoader;

    private Logger log = Logger.getLogger(Application.class.getName());

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    @Override
    public void run(String... strings) throws Exception {
        leaveLoader.loadLeaveRecordsFromCsvFile();
        log.log(Level.INFO, "Completed loading the records from the CSV File");
    }
}
