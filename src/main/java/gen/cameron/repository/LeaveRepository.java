package gen.cameron.repository;

import gen.cameron.model.Employee;
import gen.cameron.model.Leave;
import gen.cameron.model.Team;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * Created by wendycameron on 26/09/2014.
 */
public interface LeaveRepository extends CrudRepository<Leave, Long> {

    @Query("SELECT DISTINCT l FROM Leave l INNER JOIN FETCH l.employee e WHERE LOWER(e.employeeName) = LOWER(?1)")
    List<Leave> findByEmployeeName(String employeeName);
}
