package gen.cameron.repository;

import gen.cameron.model.Employee;
import gen.cameron.model.Team;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * Created by wendycameron on 26/09/2014.
 */
public interface EmployeeRepository extends CrudRepository<Employee, Long> {

    Employee findByEmployeeName(String employeeName);

    @Query("SELECT e FROM Employee e INNER JOIN FETCH e.team t WHERE LOWER(t.teamName) = LOWER(?1)")
    List<Employee> findByTeamName(String teamName);
}
