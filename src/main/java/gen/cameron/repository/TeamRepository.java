package gen.cameron.repository;

import gen.cameron.model.Team;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by wendycameron on 26/09/2014.
 */
public interface TeamRepository extends CrudRepository<Team, Long> {

    Team findByTeamName(String teamName);
}
