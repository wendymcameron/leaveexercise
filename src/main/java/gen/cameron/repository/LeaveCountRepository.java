package gen.cameron.repository;

import java.util.Date;
import java.util.List;

import gen.cameron.model.LeaveCount;
import gen.cameron.model.Team;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by wendycameron on 26/09/2014.
 */
public interface LeaveCountRepository extends CrudRepository<LeaveCount, Long> {

    @Query("SELECT lc FROM LeaveCount lc INNER JOIN FETCH lc.team t WHERE lc.day = ?1 AND LOWER(t.teamName) = LOWER(?2)")
    List<LeaveCount> findByDayAndTeam(Date day, String team);

    @Query("SELECT lc FROM LeaveCount lc INNER JOIN FETCH lc.team t WHERE LOWER(t.teamName) = LOWER(?1) AND lc.day BETWEEN ?2 AND ?3 AND lc.employeeCount > ?4 ")
    List<LeaveCount> findByTeamAndDayBetweenAndEmployeeCountGreaterThan(String team, Date startDate, Date endDate, int employeeCount);
}
