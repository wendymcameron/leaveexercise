package gen.cameron;

import org.springframework.batch.item.file.mapping.FieldSetMapper;
import org.springframework.batch.item.file.transform.FieldSet;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by wendycameron on 25/09/2014.
 */
public class LeaveRecord {

    private String employeeName;
    private String team;
    private Date startDate;
    private Date endDate;

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String getEmployeeName() {
        return employeeName;
    }

    public void setEmployeeName(String employeeName) {
        this.employeeName = employeeName;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public String getTeam() {
        return team;
    }

    public void setTeam(String team) {
        this.team = team;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        LeaveRecord that = (LeaveRecord) o;

        if (employeeName != null ? !employeeName.equals(that.employeeName) : that.employeeName != null) return false;
        if (endDate != null ? !endDate.equals(that.endDate) : that.endDate != null) return false;
        if (startDate != null ? !startDate.equals(that.startDate) : that.startDate != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = employeeName != null ? employeeName.hashCode() : 0;
        result = 31 * result + (startDate != null ? startDate.hashCode() : 0);
        result = 31 * result + (endDate != null ? endDate.hashCode() : 0);
        return result;
    }

    protected static class LeaveRecordSetMapper implements FieldSetMapper<LeaveRecord> {
        private DateFormat m_ISO8601Local = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");

        public LeaveRecord mapFieldSet(FieldSet fieldSet) {
            LeaveRecord leaveRecord = new LeaveRecord();

            leaveRecord.setEmployeeName(fieldSet.readString(0));
            leaveRecord.setTeam(fieldSet.readString(1));
            try {
                leaveRecord.setStartDate(m_ISO8601Local.parse(fieldSet.readString(2)));
                leaveRecord.setEndDate(m_ISO8601Local.parse(fieldSet.readString(3)));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            return leaveRecord;
        }
    }
}
