Leave Exercise

The problem:
Provide RESTful API to support the following:
a) List of holiday intervals (startdate, end date) for any given employee
b) List of employees for a given team
c) List of All teams in the system.
d) dates in a given period when, for a given team , more than X number of people have booked

To support developing this execise I have written a python script that generates a large 100,000 employee with
between 1 and 4 holidays and random holiday lengths of beween 1 and 20 days.  This has been generated using
random generators etc.  Also this data has only been generated for the current calendar year.

The following assumptions have been made:

1) An employee belongs to only one team.
2) An employee will take between 1 and 4 holidays per year
3) I am only using data for the current calendar year. Minimises the count data structure to support the query
   dates in a given period when, for a given team , more than X number of people have booked

The following compromises have been made to simplify the execise and be able to deliver it in 5MB file.
1) Weekends at present have not been removed.  Generated file uses random number generation which does not
   account for weekends.
2) In memory HSQL database has been used and the data set has been loaded.  For a production system there
   are a wide range of better options for solving this problem.  Also this will not scale to Millions of
   records.
3) I am using spring-boot to load up all of the dependencies to make a simple command line utility to run it.
   Also to make the development of the exercise as simple and straight forward as possible.

The following are sample request for the API.
curl -s http://localhost:8080/teams > target/teamsActual.json
curl -s http://localhost:8080/employees > target/employeesActual.json
curl -s http://localhost:8080/employees/a-team > target/a-teamActual.json
curl -s http://localhost:8080/leave/intervals/employee-x=94 > target/employee-x=94Actual.json
curl -s "http://localhost:8080/leave/intervals/team/c-team?startDate=2014/01/01&endDate=2014/06/30&absences=1" > target/absencesActual.json

Productionising the system:
Depending on where the real data is and how much data there is there is wide range of possible solutions.
1) Assuming data in Oracle and Millions of records.  A simple approach would be to generate a materialised view that generates the
   counts and refreshes periodically (possibly daily).  The program would also need to be extended to database paging for
   listing teams, employees and leave record.
2) Data much larger so gigabytes or similar and in a HDFS you would need to write a map reduce function running at regular intervals across
   the data to generate the counts to support the most complex query.  Reading the employees, teams and leave would also need
   to be proper Hadoop functions etc.